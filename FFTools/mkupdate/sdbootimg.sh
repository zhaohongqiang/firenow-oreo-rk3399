#!/bin/bash

set -e

#SDCARD="/dev/mmcblk0"

PACKAGE_FILE="package-file"

LOADER=$(cat $PACKAGE_FILE | grep -i loader | awk '{printf $2}' | sed 's/\r//g')
PARAMETER=$(cat $PACKAGE_FILE | grep -i parameter | awk '{printf $2}' | sed 's/\r//g')
curdate=`date "+%Y%m%d%H%M%S"`
#pgfilename=$(basename $(readlink ${PACKAGE_FILE}))
#SDCARDIMG=${pgfilename##sd-package-file_}_SDBOOT_${curdate}.img
SDCARDIMG=SDBOOT_${curdate}.img

if [[ $# -gt 0 ]]; then
    SDCARDIMG=$1
fi

declare -a PARTITIONS
declare -a USER_FORMAT_PARTITIONS
declare -i PARTITION_INDEX

#USER_FORMAT_PARTITIONS=(userboot linuxroot)
#USER_FORMAT_PARTITIONS=(boot)
#USER_FORMAT_PARTITIONS=(userboot)

ERROR()
{
    echo $*
    exit 1
}

DD()
{
    [ -b ${SDCARD} ] && dd $*
}

GENERATE_IDBLOCK_DATA()
{
    ./boot_merger --gensdboot $LOADER
}

CREATE_PARTITIONS_ON_SDCARD()
{
	#echo -n "$0: Calculating partition sizes for '${SDCARD}' ... "
        PARTITIONS=()
        START_OF_PARTITION=0
        PARTITION_INDEX=0
	for PARTITION in `cat ${PARAMETER} | grep '^CMDLINE' | sed 's/ //g' | sed 's/.*:\(0x.*[^)])\).*/\1/' | sed 's/,/ /g'`; do
        	PARTITION_NAME=`echo ${PARTITION} | sed 's/\(.*\)(\(.*\))/\2/'`
        	PARTITION_START=`echo ${PARTITION} | sed 's/.*@\(.*\)(.*)/\1/'`
        	PARTITION_LENGTH=`echo ${PARTITION} | sed 's/\(.*\)@.*/\1/'`

            PARTITIONS+=("$PARTITION_NAME")
            PARTITION_INDEX+=1

            eval "${PARTITION_NAME}_START_PARTITION=${PARTITION_START}"
            eval "${PARTITION_NAME}_LENGTH_PARTITION=${PARTITION_LENGTH}"
            eval "${PARTITION_NAME}_INDEX_PARTITION=${PARTITION_INDEX}"

            IMGFILE=$(cat $PACKAGE_FILE | grep -wi ${PARTITION_NAME} | awk '{printf $2}' | sed 's/\r//g')
            if [ x"$IMGFILE" != x ]; then
                if [[ -f ${IMGFILE} ]]; then
                    echo "dd if=${IMGFILE}    of=SDCARD seek=$(((${PARTITION_START} + 0x2000))) ibs=1M conv=sync,fsync"
                    dd if=${IMGFILE}    of=${SDCARDIMG} seek=$(((${PARTITION_START} + 0x2000))) ibs=1M conv=sync,fsync > /dev/null 2>&1
                fi
            fi
	done

    PREPARE_SDCARD

    for PARTITION in ${USER_FORMAT_PARTITIONS[@]}; do
        PSTART=${PARTITION}_START_PARTITION
        PLENGTH=${PARTITION}_LENGTH_PARTITION
        PINDEX=${PARTITION}_INDEX_PARTITION
        PSTART=${!PSTART}
        PLENGTH=${!PLENGTH}
        PINDEX=${!PINDEX}
        [[ ${PSTART} -eq 0 ]] && echo "Creating ${PARTITION} Partition ERROR!" && exit
        echo "Creating ${PARTITION} Partition, index ${PINDEX}, start ${PSTART}, length ${PLENGTH}"

        #PBEGIN=$(((${PSTART} + 0x2000 ) * 512 / 1024 /1024))
        PBEGIN=$(((${PSTART} + 0x2000 )))
        if [ "${PLENGTH}" == "-" ]; then
            PEND=
        else
            #PENDM=$(((${PSTART} + ${PLENGTH} -1 + 0x2000 ) * 512 / 1024 /1024))
            #PEND=${PENDM}M
            PEND=$(((${PSTART} + ${PLENGTH} -1 + 0x2000 )))
        fi

        IMG_MAX_SECROT=$(sgdisk -E ${SDCARDIMG} | sed -n "2p")
        if [[ ${PEND} > ${IMG_MAX_SECROT} ]]; then
            #PEND=${IMG_MAX_SECROT}
            PEND=
        fi

        echo "sgdisk -n ${PINDEX}:${PBEGIN}:${PEND} -t ${PINDEX}:0700 ${SDCARDIMG}"
        #sgdisk -n ${PINDEX}:${PBEGIN}:${PEND} -t ${PINDEX}:0700 ${SDCARDIMG}
        sgdisk -n ${PINDEX}:${PBEGIN}:${PEND} -t ${PINDEX}:0700 ${SDCARDIMG}
#        mkfs.ext4 -F -L ${PARTITION} ${SDCARD}p${PINDEX}

        #echo "sgdisk -n ${PINDEX}:${PBEGIN}M:${PEND} -t ${PINDEX}:0700 ${SDCARDIMG}"
        #sgdisk -n ${PINDEX}:${PBEGIN}M:${PEND} -t ${PINDEX}:0700 ${SDCARDIMG}
        #mkfs.ext4 -F -L ${PARTITION} ${SDCARDIMG}${PINDEX}

	done

    sleep 5
}

PREPARE_SDCARD()
{

    sleep 3
    sgdisk -Z ${SDCARDIMG}
    sleep 3
}

FLASH_IMAGR_TO_PARTITION()
{
    ./rkcrc -p ${PARAMETER} parameter.img
    dd if=parameter.img      of=${SDCARDIMG} seek=$(((0x000000 + 0x2000))) ibs=1M conv=sync,fsync > /dev/null 2>&1
    rm -rf parameter.img
}

FLASH_LOADER()
{
    GENERATE_IDBLOCK_DATA
    dd if=SD.bin            of=${SDCARDIMG}  seek=$(((0x000040)))  conv=sync,fsync
    rm -rf SD.bin
}


FLASH_LOADER
FLASH_IMAGR_TO_PARTITION

#[ ! -b ${SDCARD} ] && ERROR "${SDCARD} not found!"
#DD if=${SDCARDIMG} of=${SDCARD} conv=sync,fsync

CREATE_PARTITIONS_ON_SDCARD

